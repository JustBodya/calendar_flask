from flask import Flask, request

app = Flask(__name__)

from acme import model
from acme import logic

_event_logic = logic.EventLogic()

class ApiExeption(Exception):
    pass


def from_raw(raw_event: str) -> model.Event:
    parts = raw_event.split('|')
    if len(parts) == 3:
        event = model.Event()
        event.id = None
        event.title = parts[0]
        event.text = parts[1]
        event.date = parts[2]
        return event
    elif len(parts) == 4:
        event = model.Event()
        event.id = parts[0]
        event.title = parts[1]
        event.text = parts[2]
        event.date = parts[3]
        return event
    else:
        raise ApiExeption(f"Invalid RAW note data {raw_event}")
    


def to_raw(event: model.Event) -> str:
    if event.id is None:
        return f"{event.title}|{event.text}|{event.date}"
    else:
        return f"{event.id}|{event.title}|{event.text}|{event.date}"


API_ROOT = "/api/v1"
EVENT_API_ROOT = API_ROOT + "/calendar"


@app.route(EVENT_API_ROOT + "/", methods=["POST"])
def create():
    try:
        data = request.get_data().decode('utf-8')
        event = from_raw(data)
        _id =_event_logic.create(event)
        return f"new id: {_id}", 201
    except Exception as ex:
        return f"failed to CREATE with: {ex}", 500


@app.route(EVENT_API_ROOT + "/", methods=["GET"])
def list():
    try:
        events = _event_logic.list()
        raw_events = ""
        for event in events:
            raw_events += to_raw(event) + '\n'
        return raw_events, 200
    except Exception as ex:
        return f"falied to LIST with: {ex}", 500


@app.route(EVENT_API_ROOT + "/<_id>/", methods=["GET"])
def read(_id: str):
    try:
        event = _event_logic.read(_id)
        raw_event = to_raw(event)
        return raw_event, 200
    except Exception as ex:
        return f"falied to READ with: {ex}", 500


@app.route(EVENT_API_ROOT + "/<_id>/", methods=["PUT"])
def update(_id: str):
    try:
        data = request.get_data().decode('utf-8')
        event = from_raw(data)
        _event_logic.update(_id, event)
        return "updated", 200
    except Exception as ex:
        return f"falied to UPDATE with: {ex}", 500


@app.route(EVENT_API_ROOT + "/<_id>/", methods=["DELETE"])
def delete(_id: str):
    try:
        _event_logic.delete(_id)
        return "deleted", 200
    except Exception as ex:
        return f"falied to DELETE with: {ex}", 500


if __name__ == "__main__":
    app.run(debug=True)